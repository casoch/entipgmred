#include "Game.h"

/**
* Constructor
* Tip: You can use an initialization list to set its attributes
* @param windowTitle is the window title
* @param screenWidth is the window width
* @param screenHeight is the window height
*/
Game::Game(std::string windowTitle, int screenWidth, int screenHeight, std::string serverAddress, std::string clientAddress, std::string nick) :
	_windowTitle(windowTitle),
	_screenWidth(screenWidth),
	_screenHeight(screenHeight),
	_gameState(GameState::INIT),
	_network(serverAddress, clientAddress, nick)
	
{
}

/**
* Destructor
*/
Game::~Game()
{
}

/*
* Game execution
*/
void Game::run() {
		//Prepare the game components
	init();
		//Start the game if everything is ready
	gameLoop();
}

/*
* Initialize all the components
*/
void Game::init() {
	srand((unsigned int)time(NULL));
		//Create a window
	_graphic.createWindow(_windowTitle, _screenWidth, _screenHeight, false);	
	_graphic.setWindowBackgroundColor(0, 0, 0, 255);
		//Set the font style
	_graphic.setFontStyle(TTF_STYLE_NORMAL);
		//Initialize the game elements
}


/*
* Game execution: Gets input events, processes game logic and draws sprites on the screen
*/
void Game::gameLoop() {	
	_gameState = GameState::PLAY;
	
	while (_gameState != GameState::EXIT) {		
		/*clock_t startTime = clock();*/
		if (_network.GetNetworkState() == NetworkState::SAYINGHELLO)
		{
			_network.SayHello();
		}

		int positionSquare = aSquares[_network.GetIdSquare()].GetPosition();
		_network.SendMove(positionSquare, _inputState, _inputStateList);
		
		Receiving();
		SimulateOtherPlayers();
		//Update the game physics
		doPhysics();
		//Detect keyboard and/or mouse events
		_graphic.detectInputEvents();
		//Execute the player commands 
		executePlayerCommands();
		//Render game
		renderGame();	
		/*clock_t endTime = clock();
		std::cout << "ITERACION: " << (endTime-startTime) << std::endl;*/
	}
}

void Game::Receiving()
{
	std::string strReceived;
	int numBytes = _network.Receive(strReceived);
	
	if (numBytes > 0)
	{
		//std::cout << "Recibo: " << strReceived << std::endl;
		int index_ = strReceived.find_first_of('_');
		std::string cabecera = strReceived.substr(0, index_);
		std::string contenido = strReceived.substr(index_ + 1, strReceived.size() - index_);
		
		if (cabecera == HEADER_WELCOME)
		{
			_network.SetIdSquare(atoi(contenido.c_str()));
			//std::cout << "Estado a WELCOMED" << std::endl;
			_network.SetNetworkState(NetworkState::WELCOMED);
		}
		else if (cabecera == HEADER_FULL)
		{

		}
		else if (cabecera == HEADER_POSITION)
		{
			std::cout << "Recibo " << strReceived << std::endl;
			//std::vector<std::string> aStrPositions = NetworkClient::SplitPositionMessage(strReceived);

			//for (size_t i = 0; i < aStrPositions.size(); i++)
			//{
				/*int index_ = aStrPositions[i].find_first_of('_');
				std::string strIdAck = aStrPositions[i].substr(0, index_);
				std::string contentPosition = aStrPositions[i].substr(index_ + 1, aStrPositions[i].size() - index_);*/
				int index_ = contenido.find_first_of('_');
				std::string strIdAck = contenido.substr(0, index_);
				std::string contentPosition = contenido.substr(index_ + 1, contenido.size() - index_);

				index_ = contentPosition.find_first_of('_');
				std::string strIdSquare = contentPosition.substr(0, index_);
				std::string strPositionsWithTotal = contentPosition.substr(index_ + 1, contentPosition.size() - index_);

				int indexPad = strPositionsWithTotal.find_first_of('#');
				std::string strPositions = strPositionsWithTotal.substr(0, indexPad);
				std::string strTotal = strPositionsWithTotal.substr(indexPad + 1, strPositionsWithTotal.size() - indexPad);

				int idSquare = atoi(strIdSquare.c_str());
				int total = atoi(strTotal.c_str());
				std::vector<int> aPositions;
				if (strPositions.size() > 0)
				{
					aPositions = LittleSquareClient::ParserListPositions(strPositions);
				}
				int deltaMovement = LittleSquare::CalculateDeltaMovement(strPositions);

				if (idSquare == _network.GetIdSquare())
				{
					//Sacamos el InputState de la lista
					int idAck = atoi(strIdAck.c_str());
					if (_inputStateList.Size() > 1)
					{
						std::string aaa = "";
					}
					bool okPosition = _inputStateList.Acknowledge(idAck, total);

					if (!okPosition)
					{
						//Ahora s�lo corrijo si la predicci�n esta mal.
						aSquares[idSquare].SetPosition(total);
						std::cout << ">> cuadrado a " << total << std::endl;
					}
				}
				else
				{
					//Cuando me env�an la posici�n de otro jugador.
					//Encolo los movimientos en el aPlayersMoves
					
					/*Si interpolamos seg�n la forma 1
					int endPos = LittleSquareClient::CalculateEndPosition(aSquares[idSquare].GetPosition(), _aPlayersMoves);
					LittleSquareClient::InterpolatePath(endPos, total, aPositions);
					if (aPositions.size() > 0)
					{
					if (aPositions.size() > NUM_STEPS_ENTITY)
					{
					aPositions = LittleSquareClient::CompressPath(NUM_STEPS_ENTITY, aPositions);
					}
					_aPlayersMoves.AddMoves(idSquare, aPositions);
					}
					*/
					if (aPositions.size() > 0)
					{
						/*//Interpolaci�n forma2 
						if (aPositions.size() > NUM_STEPS_ENTITY)
						{
							aPositions = LittleSquareClient::CompressPath(NUM_STEPS_ENTITY, aPositions);
						}*/
						_aPlayersMoves.AddMoves(idSquare, aPositions);
					}
				

					//Ya no coloco la posici�n del otro jugador nada m�s llegar. Quiero simularlo poco a poco.
					//aSquares[idSquare].SetPosition(total);
					//std::cout << "El cuadrado del otro est� en " << aSquares[idSquare].GetPosition() << std::endl;
					/*Ahora ya no pongo la posici�n del otro cuadrado directamente
					if (strPositions.size() == 0)
					{
						aSquares[idSquare].SetPosition(total);
					}*/
				}

				std::cout << "Confirman: " << total << std::endl;
			//}

		}
		else if (cabecera == HEADER_EXIT)
		{
			_gameState = GameState::EXIT;
		}
	}
}

void Game::SimulateOtherPlayers()
{
	PlayerMove playerMove;
	bool okMove = _aPlayersMoves.PopMove(playerMove);
	if (okMove)
	{
		aSquares[playerMove.GetIdSquare()].SetDelta(playerMove.GetDelta());
	}
}

/**
* Executes the actions sent by the user by means of the keyboard and mouse
* Reserved keys:
- up | left | right | down moves the player object
- m opens the menu
*/
void Game::executePlayerCommands() {

	//Controlo que no se env�en movimientos inv�lidos.
	//Aunque si se enviaran el servidor lo controlar�a y me devolver�a al sitio.
	if (_graphic.isKeyDown(SDLK_RIGHT)){
		if (aSquares[_network.GetIdSquare()].GetPosition() + 1 <= MAX_SQUARE)
		{
			_inputState.AddRight();
			aSquares[_network.GetIdSquare()].AddRight();
		}
	}
	if (_graphic.isKeyDown(SDLK_LEFT)) {
		if (aSquares[_network.GetIdSquare()].GetPosition() - 1 >= MIN_SQUARE)
		{
			_inputState.AddLeft();
			aSquares[_network.GetIdSquare()].AddLeft();
		}
	}
	if (_graphic.isKeyPressed(SDLK_ESCAPE)) {
		_gameState = GameState::EXIT;
		_network.Send("EXIT");
	}
}

/*
* Execute the game physics
*/
void Game::doPhysics() {
	

	
}

/**
* Render the game on the screen
*/
void Game::renderGame() {
		//Clear the screen
	_graphic.clearWindow();

		//Draw the screen's content based on the game state
	if (_gameState == GameState::MENU) {
		drawMenu();
	}
	else {
		drawGame();
	}
		//Refresh screen
	_graphic.refreshWindow();
}

/*
* Draw the game menu
*/
void Game::drawMenu() {

}

/*
* Draw the game, winner or loser screen
*/
void Game::drawGame() {

	_graphic.drawFilledRectangle(BLUE, 0, 0, 20, 300);
	_graphic.drawFilledRectangle(BLUE, 680, 0, 20, 300);
	
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		int position = aSquares[i].GetPosition();
		_graphic.drawFilledRectangle(i, position, 40 + 180*i, SIZE_SQUARE, SIZE_SQUARE);
	}
}

/*
* Draw the sprite on the screen
* @param sprite is the sprite to be displayed
*/
void Game::drawSprite(Sprite & sprite) {
	
}