#pragma once
#include <string>
#include <vector>
#include "GameClientConstants.h"


class InputState
{
private:
	int absolutePosition;
	int delta;
	std::vector<int> aMoves;
	int id;

public:
	InputState():absolutePosition(0),delta(0),id(0)
	{
		
	}

	InputState(const InputState& _inputState)
	{
		aMoves = _inputState.aMoves;
		delta = _inputState.delta;
		id = _inputState.id;
		absolutePosition = _inputState.absolutePosition;
	}

	int GetId()
	{
		return id;
	}

	void SetId(int _id)
	{
		id = _id;
	}

	int GetDelta()
	{
		return delta;
	}

	void SetDelta(int _delta)
	{
		delta = _delta;
	}

	int GetAbsolutePosition()
	{
		return absolutePosition;
	}

	void SetAbsolutePosition(int _absolutePosition)
	{
		absolutePosition = _absolutePosition;
	}

	void AddLeft()
	{
		delta--;
		aMoves.push_back(-1);
		//std::cout << "Izquierda: " << moveHorizontal << std::endl;
	}

	void AddRight()
	{
		delta++;
		aMoves.push_back(1);
		//std::cout << "Derecha: " << moveHorizontal << std::endl;
	}

	/*int GetMove()
	{
		return moveHorizontal;
	}*/

	std::vector<int> GetMoves()
	{
		return aMoves;
	}

	bool Serialize(std::string& _move)
	{
		int size = aMoves.size();
		if (size != 0)
		{
			_move = std::string(HEADER_TRYPOSITION) + "_" + std::to_string(id) + "_";
			for (size_t i = 0; i < size; i++)
			{
				_move += std::to_string(aMoves[i]);
				if (i + 1 < size)
				{
					_move += ":";
				}
			}
			
			return true;
		}
		return false;
	}

	void ResetMove()
	{
		//moveHorizontal = 0;
		aMoves.clear();
		delta = 0;
	}

	~InputState() {}
};

