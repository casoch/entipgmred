#pragma once
#include <vector>
class PlayerCommand
{
private:
	int idPlayer;
	int idMove;
	int absolutePosition;
	std::vector<int> aSteps;

public:
	PlayerCommand();
	PlayerCommand(int _idPlayer, int _idMove, int _absolutePosition, std::vector<int> _aDetailedPath);
	PlayerCommand(const PlayerCommand& _playerCommand);
	int GetIdPlayer();
	int GetIdMove();
	int GetAbsolutePosition();
	std::vector<int> GetDetailedPath();
	~PlayerCommand();
};

