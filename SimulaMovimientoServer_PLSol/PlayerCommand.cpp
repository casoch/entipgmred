#include "PlayerCommand.h"



PlayerCommand::PlayerCommand()
{
}

PlayerCommand::PlayerCommand(int _idPlayer, int _idMove, int _absolutePosition, std::vector<int> _aDetailedPath): idPlayer(_idPlayer), idMove(_idMove),
	absolutePosition(_absolutePosition), aSteps(_aDetailedPath)
{
}

PlayerCommand::PlayerCommand(const PlayerCommand & _playerCommand):
	PlayerCommand(_playerCommand.idPlayer, _playerCommand.idMove, _playerCommand.absolutePosition, _playerCommand.aSteps)
{
}

int PlayerCommand::GetIdPlayer()
{
	return idPlayer;
}

int PlayerCommand::GetIdMove()
{
	return idMove;
}

int PlayerCommand::GetAbsolutePosition()
{
	return absolutePosition;
}

std::vector<int> PlayerCommand::GetDetailedPath()
{
	return aSteps;
}


PlayerCommand::~PlayerCommand()
{
}
