#pragma once

#define MAX_BUFFER 1300

#define MAX_PLAYERS 2
#define MIN_SQUARE 20
#define MAX_SQUARE 640


enum PacketType
{
	PT_HELLO, PT_WELCOME, PT_FULL, PT_TRYPOSITION, PT_POSITION, PT_DISCONNECT, PT_RESETPLAYER
};