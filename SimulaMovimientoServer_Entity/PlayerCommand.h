#pragma once
#include <string>
class PlayerCommand
{
private:
	int idPlayer;
	int idMove;
	int delta;
	std::string detailedPath;

public:
	PlayerCommand();
	PlayerCommand(int _idPlayer, int _idMove, int _delta, std::string _detailedPath);
	PlayerCommand(const PlayerCommand& _playerCommand);
	int GetIdPlayer();
	int GetIdMove();
	int GetDelta();
	std::string GetDetailedPath();
	~PlayerCommand();
};

