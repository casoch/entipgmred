#include "PlayerCommand.h"



PlayerCommand::PlayerCommand()
{
}

PlayerCommand::PlayerCommand(int _idPlayer, int _idMove, int _delta, std::string _detailedPath): idPlayer(_idPlayer), idMove(_idMove),
	delta(_delta), detailedPath(_detailedPath)
{
}

PlayerCommand::PlayerCommand(const PlayerCommand & _playerCommand):
	PlayerCommand(_playerCommand.idPlayer, _playerCommand.idMove, _playerCommand.delta, _playerCommand.detailedPath)
{
}

int PlayerCommand::GetIdPlayer()
{
	return idPlayer;
}

int PlayerCommand::GetIdMove()
{
	return idMove;
}

int PlayerCommand::GetDelta()
{
	return delta;
}

std::string PlayerCommand::GetDetailedPath()
{
	return detailedPath;
}


PlayerCommand::~PlayerCommand()
{
}
