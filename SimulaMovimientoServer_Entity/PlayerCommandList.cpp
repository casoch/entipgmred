#include "PlayerCommandList.h"
#include <LittleSquare.h>
#include <iostream>


PlayerCommandList::PlayerCommandList()
{
}

/**
 * _str es el contenido del Dispatch_Message
 */
void PlayerCommandList::AddCommand(int _idPlayer, int _idMove, int _delta, std::string _detailedPath)
{
	PlayerCommand playerCommand(_idPlayer, _idMove, _delta, _detailedPath);
	push_back(playerCommand);
}

bool PlayerCommandList::PopCommand(PlayerCommand & _playerCommand)
{
	if (size() == 0)
	{
		return false;
	}
	_playerCommand = at(0);
	erase(begin(), begin() + 1);
	return true;
}


PlayerCommandList::~PlayerCommandList()
{
}
