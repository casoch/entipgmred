#pragma once
#include <vector>
#include "PlayerCommand.h"

class PlayerCommandList: private std::vector<PlayerCommand>
{
public:
	PlayerCommandList();
	void AddCommand(int _idPlayer, int _idMove, int _delta, std::string _detailedPath);
	bool PopCommand(PlayerCommand& _playerCommand);
	~PlayerCommandList();
};

