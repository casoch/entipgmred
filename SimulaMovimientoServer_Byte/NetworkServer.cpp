#include "NetworkServer.h"
#include "GameServerConstants.h"
#include <LittleSquare.h>
#include <OutputMemoryStream.h>
#include <InputMemoryStream.h>

NetworkServer::NetworkServer(std::string _strServerAddress): timeOfLastForward(0)
{
	SocketAddress saServer;
	saServer.SetAddress(_strServerAddress);
	udpSocket.Bind(saServer);
	udpSocket.NonBlocking(true);
	for (int i = 0; i < MAX_PLAYERS; i++)
	{
		aPlayersConnected[i] = false;
	}
}

bool NetworkServer::Receive()
{
	bool exit = false;
	char buffer[MAX_BUFFER];
	SocketAddress from;

	int numBytes = udpSocket.ReceiveFrom(buffer, MAX_BUFFER, from);
	if (numBytes > 0)
	{
		if (numBytes < MAX_BUFFER)
		{
			buffer[numBytes] = '\0';
		}
		exit = Dispatch_Message(buffer, numBytes, from);
	}
	return exit;
}

void NetworkServer::SendToAll(char* _message, int _size)
{
	for (size_t i = 0; i < MAX_PLAYERS; i++)
	{
		if (aPlayersConnected[i])
		{
			SocketAddress saClient = aPlayers[i].GetSocketAddress();
			udpSocket.SendTo(_message, _size, saClient);
		}
	}
}

NetworkServer::~NetworkServer()
{

}

bool NetworkServer::Dispatch_Message(char* _message, int _sizeMessage, SocketAddress _saClient)
{
	InputMemoryStream ims(_message, _sizeMessage);
	PacketType pt;
	ims.Read(&pt);

	ClientProxy cp(_saClient);
	int index = ExistClientProxy(cp);
	int freePosition = GetPositionFreePlayer();
	
	if (pt == PacketType::PT_HELLO)
	{
		if (freePosition == -1)
		{
			OutputMemoryStream oms;
			oms.Write(PacketType::PT_FULL);
			udpSocket.SendTo(oms.GetBufferPtr(), oms.GetLength(), _saClient);
		}
		else
		{
			if (index == -1)
			{
				std::string nick = ims.ReadString();
				cp.SetNick(nick);
				aPlayers[freePosition] = cp;
				aPlayersConnected[freePosition] = true;
				
				OutputMemoryStream oms;
				oms.Write(PacketType::PT_WELCOME);
				//Corresponde al idSquare
				oms.Write(freePosition);
				int numPlayers = GetNumPlayers()-1;
				//Cu�nta informaci�n recibir� de los dem�s usuarios
				//Preparado para m�s de 2
				oms.Write(numPlayers);
				for (int i = 0; i < MAX_PLAYERS; i++)
				{
					if (aPlayersConnected[i] && i != freePosition)
					{
						//Por cada uno de los dem�s jugadores paso 
						//su idSquare y su posici�n.
						oms.Write(i);
						oms.Write(aPlayers[i].GetPositionSquare());
					}
				}
				udpSocket.SendTo(oms.GetBufferPtr(), oms.GetLength(), _saClient);
			}
		}
	}
	else if (pt == PacketType::PT_TRYPOSITION)
	{
		if (index != -1)
		{
			int idMove;
			ims.Read(&idMove);
			std::vector<int> aPositions;
			ims.Read(&aPositions);
			int delta = LittleSquare::CalculateDeltaMovement(aPositions);
			
			aPlayersCommands.AddCommand(index, idMove, delta, aPositions);
			
		}
	}
	else if (pt == PacketType::PT_DISCONNECT)
	{
		if (index != -1)
		{
			aPlayersConnected[index] = false;
			OutputMemoryStream oms;
			oms.Write(PacketType::PT_RESETPLAYER);
			oms.Write(index);
			SendToAll(oms.GetBufferPtr(), oms.GetLength());
		}
		if (GetNumPlayers() == 0)
		{
			return true;
		}
	}
	return false;
}

void NetworkServer::Dispatch_Forwards()
{
	clock_t time = clock();
	if (time > timeOfLastForward + FREQUENCY_SENDING_WORLD)
	{
		OutputMemoryStream oms;
		oms.Write(PacketType::PT_POSITION);
		oms.Write(aPlayersCommands.Size());
		for (size_t i = 0; i < aPlayersCommands.Size(); i++)
		{
			PlayerCommand playerCommand;
			aPlayersCommands.PopCommand(playerCommand);
			
			int idPlayer = playerCommand.GetIdPlayer();
			int delta = playerCommand.GetDelta();
			std::cout << "Intenta " << std::to_string(delta) << std::endl;
			int newPosition = aPlayers[idPlayer].ChangeMove(delta);
			std::cout << "Se concede " << std::to_string(newPosition) << std::endl;

			oms.Write(playerCommand.GetIdMove());
			oms.Write(idPlayer);
			if (!aPlayers[idPlayer].GetErrorLastMove())
			{
				oms.Write(playerCommand.GetDetailedPath());

			}
			else
			{
				oms.Write(0);
			}
			oms.Write(newPosition);
		}
		SendToAll(oms.GetBufferPtr(), oms.GetLength());
		timeOfLastForward = time;
	}
}

int NetworkServer::ExistClientProxy(ClientProxy _clientProxy)
{
	for (size_t i = 0; i < MAX_PLAYERS; i++)
	{
		if (aPlayersConnected[i] && aPlayers[i] == _clientProxy)
		{
			return i;
		}
	}
	return -1;
}

int NetworkServer::GetNumPlayers()
{
	int numPlayers = 0;
	for (size_t i = 0; i < MAX_PLAYERS; i++)
	{
		if (aPlayersConnected[i] == true)
		{
			numPlayers++;
		}
	}
	return numPlayers;
}

int NetworkServer::GetPositionFreePlayer()
{
	int numPlayers = GetNumPlayers();
	if (numPlayers < MAX_PLAYERS)
	{
		for (size_t i = 0; i < MAX_PLAYERS; i++)
		{
			if (!aPlayersConnected[i])
			{
				return i;
			}
		}
	}
	return -1;
}
