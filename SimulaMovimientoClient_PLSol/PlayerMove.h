#pragma once
class PlayerMove
{
private:
	int delta;
	int position;
	int idSquare;
public:
	PlayerMove();
	PlayerMove(int _delta, int _position, int _idSquare);
	PlayerMove(const PlayerMove& _playerMove);
	int GetDelta();
	int GetPosition();
	int GetIdSquare();
	void SetDelta(int _delta);
	void SetPosition(int _position);
	void SetIdSquare(int _idSquare);
	~PlayerMove();
};

