#include "NetworkClient.h"
#include <OutputMemoryStream.h>
#include <OutputMemoryBitStream.h>
#include <SimulaMovimientoTools.h>
#include <InputMemoryBitStream.h>

NetworkClient::NetworkClient(std::string _addressServer, std::string _addressClient, std::string _nick):nick(_nick), networkState(NetworkState::UNINITIALIZED)
{
	saServer.SetAddress(_addressServer);
	SocketAddress myAddress;
	myAddress.SetAddress(_addressClient);
	int errBind = udpSocket.Bind(myAddress);
	int errBlock = udpSocket.NonBlocking(true);
	if (errBind > -1 && errBlock > -1)
	{
		//std::cout << "El estado de cliente pasa a SAYING HELLO" << std::endl;
		networkState = NetworkState::SAYINGHELLO;
	}
}

NetworkState NetworkClient::GetNetworkState()
{
	return networkState;
}

void NetworkClient::SetNetworkState(NetworkState _networkState)
{
	networkState = _networkState;
}

int NetworkClient::GetIdSquare()
{
	return idSquare;
}

void NetworkClient::SetIdSquare(int _idSquare)
{
	idSquare = _idSquare;
}

void NetworkClient::SayHello()
{
	clock_t time = clock();
	if (time > timeOfLastHello + FREQUENCY_SAYING_HELLO)
	{
		OutputMemoryBitStream ombs;
		ombs.Write(PacketType::PT_HELLO, 3);
		ombs.WriteString(nick);
		Send(ombs.GetBufferPtr(), ombs.GetByteLength());
		timeOfLastHello = time;
	}
}

void NetworkClient::SendMove(int _positionSquare, InputState& _inputState, InputStateList& _inputStateList)
{
	clock_t time = clock();
	if (time > timeOfLastMove + FREQUENCY_SENDING_INPUTS)
	{
		if (_inputState.GetDelta() != 0)
		{
			//Hay movimiento que enviar --> Si es cero, me quedo donde estoy. No vale la pena perder tiempo en enviar y en esperar respuesta.
			_inputState.SetId(_inputStateList.GetCounter());
			std::vector<int> aMoves = _inputState.GetMoves();
			_inputState.SetAbsolutePosition(_positionSquare);
			_inputStateList.Add(_inputState);

			OutputMemoryBitStream ombs;
			ombs.Write(PacketType::PT_TRYPOSITION, 3);
			ombs.Write(_inputState.GetId());
			ombs.Write(_positionSquare, 10);
			//ombs.Write(aMoves);
			std::cout << "Estoy en " << _positionSquare << std::endl;

			Send(ombs.GetBufferPtr(), ombs.GetByteLength());
			_inputState.ResetMove();
		}
		timeOfLastMove = time;
	}
}



void NetworkClient::Send(char* _message, int _size)
{
	//std::cout << "Env�o: " << _message.c_str() << std::endl;
	udpSocket.SendTo(_message, _size, saServer);
}

int NetworkClient::Receive(char* _message)
{
	SocketAddress from;
	int bytesReceived = udpSocket.ReceiveFrom(_message, MAX_BUFFER, from);
	if (bytesReceived > 0)
	{
		if (bytesReceived < MAX_BUFFER)
		{
			_message[bytesReceived] = '\0';
		}
		float rndPacketLoss = SimulaMovimientoTools::GetRandomFloat();
		if (rndPacketLoss < PERCENT_PACKETLOSS)
		{
			InputMemoryBitStream imbs(_message, bytesReceived);
			PacketType pt = PacketType::PT_EMPTY;
			imbs.Read(&pt, 3);
			std::cout << rndPacketLoss << "Simulamos que se pierde "<< _message << " - "<<bytesReceived<<" - "<<"de tipo "<< pt << std::endl;
			return -1;
		}
	}
	return bytesReceived;
}



NetworkClient::~NetworkClient()
{
}
