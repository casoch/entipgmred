#include "LittleSquareClient.h"
#include "GameClientConstants.h"
#include <iostream>

bool LittleSquareClient::IsValidPosition()
{
	if (position >= MIN_SQUARE && position <= MAX_SQUARE)
	{
		return true;
	}
	return false;
}

void LittleSquareClient::SetDelta(int _delta)
{
	//Compruebo el delta que ha hecho el otro jugador, para no simular posiciones err�neas
	//Lo corrijo aqu� para ver qu� pasar�a si el otro jugador tratara de hacer trampas.
	//Adem�s se podr�a a�adir que no se permitiera el movimiento m�s all� de los l�mites.
	//int checkPosition = position + _delta;
	//if (checkPosition >= MIN_SQUARE && checkPosition <= MAX_SQUARE)
	//{
		position += _delta;
	//}
}

int LittleSquareClient::CalculateEndPosition(int _currentPosition, PlayerMoveList & _aPlayersMoves)
{
	for (size_t i = 0; i < _aPlayersMoves.size(); i++)
	{
		PlayerMove playerMove = _aPlayersMoves[i];
		_currentPosition += playerMove.GetDelta();
	}
	return _currentPosition;
}

void LittleSquareClient::InterpolatePath(int _start, int _end, std::string& _strPositions)
{
	_strPositions = "";
	if (_start < _end)
	{
		for (int i = _start; i < _end; i++)
		{
			_strPositions += std::to_string(1);
			if (i + 1 < _end)
			{
				_strPositions += ":";
			}
		}
	}
	else if (_end < _start)
	{
		for (int i = _end; i < _start; i++)
		{
			_strPositions += std::to_string(-1);
			if (i + 1 < _start)
			{
				_strPositions += ":";
			}
		}
	}

}

void LittleSquareClient::InterpolatePath(int _start, int _end, std::vector<int>& _aPositions)
{
	std::cout << "Genero de " << _start << " a " << _end << std::endl;
	_aPositions.clear();
	int numLittleSteps = abs(_start - _end);
	if (numLittleSteps < NUM_STEPS_ENTITY)
	{
		for (size_t i = 0; i < numLittleSteps; i++)
		{
			if (_start < _end)
			{
				_aPositions.push_back(_start + 1 + i);
			}
			else
			{
				_aPositions.push_back(_start -1 - i);
			}
		}
	}
	else
	{
		//De cu�nto hago los pasos
		int sizeStep = ceil(numLittleSteps / NUM_STEPS_ENTITY);
		int acum = _start;
		if (_start < _end)
		{
			while (acum < _end)
			{
				_aPositions.push_back(acum + sizeStep);
				acum += sizeStep;
			}
		}
		else
		{
			while (acum > _end)
			{
				_aPositions.push_back(acum - sizeStep);
				acum -= sizeStep;
			}
		}
	}
}

std::vector<int> LittleSquareClient::CompressPath(int _numSteps, std::vector<int>& _aPositions)
{
	std::vector<int> aCompressPath;
	int numCells = _aPositions.size();
	int cellPerStep = ((numCells) / (_numSteps))+1;
	
	int currentIndex = 0;
	while (true)
	{

		int acum = 0;
		for (int i = currentIndex; i < (currentIndex+cellPerStep) && i<numCells; i++)
		{
			acum += _aPositions[i];
		}
		aCompressPath.push_back(acum);
		currentIndex = currentIndex + cellPerStep;
		if (currentIndex >= numCells)
		{
			break;
		}
	}
	return aCompressPath;
}

std::vector<int> LittleSquareClient::ParserListPositions(std::string _positions)
{
	std::vector<int> aPositions;
	if (_positions.size() == 0)
		return aPositions;
	int index2Ptos = _positions.find_first_of(':');
	if (index2Ptos == std::string::npos)
	{
		aPositions.push_back(atoi(_positions.c_str()));
	}
	while (index2Ptos != std::string::npos)
	{
		std::string strPosition = _positions.substr(0, index2Ptos);
		std::string strTheOthers = _positions.substr(index2Ptos + 1, _positions.size() - index2Ptos);
		aPositions.push_back(atoi(strPosition.c_str()));
		index2Ptos = strTheOthers.find_first_of(':');
		if (index2Ptos == std::string::npos)
		{
			aPositions.push_back(atoi(strTheOthers.c_str()));
		}
		_positions = strTheOthers;
	} 
	
	return aPositions;
}
